const initialState = {
  card: {
    number1: '',
    number2: '',
    number3: '',
    number4: '',
    holder: '',
    month: '',
    year: '',
    ccv: ''
  },
  amount: '',
  commission: 0,
  loader: false,
  agreed: false,
  readonly: false
}

export default {
  state: initialState,
  getters: {
    loader (state) {
      return state.loader
    },
    card (state) {
      return state.card
    },
    amount (state) {
      return state.amount
    },
    commission (state) {
      return state.commission
    },
    agreed (state) {
      return state.agreed
    },
    readonly (state) {
      return state.readonly
    }
  },
  mutations: {
    set (state, payload) {
      state[payload.name] = payload.value
    },
    setCard (state, payload) {
      state.card[payload.name] = payload.value
    },
    clear (state) {
      state = initialState
    }
  },
  actions: {
    update ({commit}, payload) {
      commit('set', payload)
    },
    updateCard ({commit}, payload) {
      commit('setCard', payload)
    },
    clearAll ({commit}) {
      commit('clear')
    }
  }
}
