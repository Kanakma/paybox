import Vue from 'vue'
import Router from 'vue-router'
import Success from '@/components/Success'
import Error from '@/components/Error'
import Pay from '@/components/Pay'

Vue.use(Router)

export default new Router({
  mode: 'history',
  routes: [
    {
      path: '/',
      name: 'pay',
      component: Pay
    },
    {
      path: '/error',
      name: 'error',
      component: Error
    },
    {
      path: '/success',
      name: 'success',
      component: Success
    }
  ]
})
